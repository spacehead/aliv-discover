﻿using App.UI.Api;
using Zenject;

namespace App.Signals
{
    public class OpenPopupSignal : Signal<OpenPopupSignal, string> { }
    public class OpenScreenSignal : Signal<OpenScreenSignal, string> { }
    public class ClosePopupSignal : Signal<ClosePopupSignal, IMediator> { }
    public class WaitClosePopupSignal : Signal<WaitClosePopupSignal, IMediator> { }
    public class ScreenToBackgroundSignal : Signal<ScreenToBackgroundSignal, bool> { }

    public class CloseScreenSignal : Signal<CloseScreenSignal> { }
    public class CloseAllPopupsSignal : Signal<CloseAllPopupsSignal> { }

    public class PopupAnimationSignal : Signal<PopupAnimationSignal, bool> { }
    public class ScreenAnimationSignal : Signal<ScreenAnimationSignal, bool> { }
}