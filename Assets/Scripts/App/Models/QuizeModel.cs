﻿using UnityEngine;
using System.Collections.Generic;
using App.Models.Data;

namespace App.Models
{
    public class QuizeModel
    {
        public List<Quize> Quizes;

        public Quize CurrentQuize;

        public QuizeModel()
        {
            Quizes = new List<Quize>();

            Quize quize_1 = new Quize();
            quize_1.Questions = new List<Question>();
            Question q1 = new Question();
            q1.AmountPoints = 10;
            q1.QuestionTitle = "World War I began in which year?";
            q1.Variants = new string[] {
            "1923",
            "1938",
            "1917",
            "1914" };
            q1.CorrectVariant = "1938";
            quize_1.Questions.Add(q1);
            
            Question q2 = new Question();
            q2.AmountPoints = 10;
            q2.QuestionTitle = "Adolf Hitler was born in which country?";
            q2.Variants = new string[] {
            "France",
            "Germany",
            "Austria",
            "Hungary" };
            q2.CorrectVariant = "Germany";
            quize_1.Questions.Add(q2);

            Question q3 = new Question();
            q3.AmountPoints = 20;
            q3.QuestionTitle = "John F. Kennedy was assassinated in";
            q3.Variants = new string[] {
            "New York",
            "Austin",
            "Dallas",
            "Miami" };
            q3.CorrectVariant = "New York";
            quize_1.Questions.Add(q3);

            Quizes.Add(quize_1);

            CurrentQuize = quize_1;
        }
    }
}