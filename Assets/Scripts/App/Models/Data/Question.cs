﻿namespace App.Models.Data
{
    public class Question
    {
        public int AmountPoints;
        public string QuestionTitle;
        public string CorrectVariant;
        public string[] Variants;
    }
}