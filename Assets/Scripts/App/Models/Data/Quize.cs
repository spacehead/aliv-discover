﻿using System.Collections.Generic;

namespace App.Models.Data
{
    public class Quize
    {
        public int Id;
        public List<Question> Questions;

        public Quize()
        {
            Questions = new List<Question>();
        }
    }
}