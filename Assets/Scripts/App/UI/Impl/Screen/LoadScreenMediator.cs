﻿using App.UI.Data;
using App.UI.Impl.Base;
using Zenject;

namespace App.UI.Impl.Screen
{
    public class LoadScreenMediator : AScreenMediator<LoadScreenView>
    {

        private void Start()
        {
            Show();
        }

        public override void Show()
        {
        }
    }
}