﻿using App.Signals;
using App.UI.Api;
using Zenject;

namespace App.UI.Impl.Base
{
    public abstract class ANativePopupMediator : ANativeMediator
    {
        [Inject]
        protected PopupAnimationSignal animationSignal;
        
        //[Inject]
        //protected NativePopupWrapper nativePopups;

        public override void Appear()
        {
            animationSignal.Fire(false);
        }

        public override void Disappear()
        {
            animationSignal.Fire(false);
        }
    }
}
