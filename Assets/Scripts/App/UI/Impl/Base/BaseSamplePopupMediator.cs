﻿namespace App.UI.Impl.Base
{
    public abstract class BaseSamplePopupMediator<T> : APopupMediator<T> where T : BaseSamplePopupView
    {
        public override void Initialize()
        {
            base.Initialize();
        }
        
        public override void Dispose()
        {
            base.Dispose();
        }

        public override void Appear()
        {
            Show();
            animationSignal.Fire(false);
        }

        public override void Disappear()
        {
            Hide();
            animationSignal.Fire(false);
        }
    }
}
