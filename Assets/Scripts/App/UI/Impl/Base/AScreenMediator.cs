﻿using App.Signals;
using App.UI.Api;
using Zenject;

namespace App.UI.Impl.Base
{
    public abstract class AScreenMediator<T> : AMonoMediator<T> where T : AView
    {
        [Inject]
        protected ScreenAnimationSignal animationSignal;

        public override void Appear()
        {
            Show();
            animationSignal.Fire(false);
        }

        public override void Disappear()
        {
            Hide();
        }
    }
}