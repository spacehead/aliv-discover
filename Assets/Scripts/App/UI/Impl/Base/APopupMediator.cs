﻿using App.Signals;
using App.UI.Api;
using Zenject;

namespace App.UI.Impl.Base
{
    public abstract class APopupMediator<T> : AMonoMediator<T> where T : AView
    {
        [Inject]
        protected PopupAnimationSignal animationSignal;
    }
}