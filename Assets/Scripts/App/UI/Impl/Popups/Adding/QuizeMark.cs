﻿using App.UI.Impl.Popups.Data;
using UnityEngine;
using UnityEngine.UI;

namespace App.UI.Impl.Popups.Adding
{
    public class QuizeMark : MonoBehaviour
    {
        public Sprite[] SpriteState;
        public GameObject PointsPanel;

        public Text PointsLabel;
        public Image BackgroudImage;

        public void SetPoints(int value)
        {
            PointsLabel.text = value.ToString();
        }

        public void SetMarkState(QuizeState state)
        {
            BackgroudImage.enabled = state != QuizeState.Nope;
            PointsPanel.SetActive(state == QuizeState.Valid);
            BackgroudImage.sprite = SpriteState[(state == QuizeState.Valid ? 1 : 0)];
        }
    }
}
