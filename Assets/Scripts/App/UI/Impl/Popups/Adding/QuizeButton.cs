﻿using UnityEngine;
using UnityEngine.UI;
using App.UI.Impl.Popups.Data;

namespace App.UI.Impl.Popups.Adding
{
    public class QuizeButton : MonoBehaviour
    {
        public Color[] ColorStates;
        public GameObject[] Marks;

        public Text NameVariant;
        public Image Background;
        public Button ActiveBtn;

        public void SetVisible(bool state)
        {
            gameObject.SetActive(state);
        }

        public void SetInteractable(bool state)
        {
            ActiveBtn.interactable = state;
        }

        public void SetVariantText(string value)
        {
            NameVariant.text = value;
        }

        public void SetState(QuizeState state)
        {
            ActivateMark(state);
            Background.color = GetColorBack(state);
        }

        private Color GetColorBack(QuizeState state)
        {
            switch (state)
            {
                case QuizeState.Invalid:
                    return ColorStates[0];
                case QuizeState.Valid:
                    return ColorStates[1];
                default:
                    return Color.white;
            }
        }

        private void ActivateMark(QuizeState state)
        {
            Marks[0].SetActive(state == QuizeState.Invalid);
            Marks[1].SetActive(state == QuizeState.Valid);
        }
    }
}