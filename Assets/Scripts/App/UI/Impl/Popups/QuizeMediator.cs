﻿using App.Models;
using App.Models.Data;
using App.UI.Impl.Base;
using System.Collections;
using Zenject;
using UnityEngine;
using App.UI.Impl.Popups.Data;

namespace App.UI.Impl.Popups
{
    public class QuizeMediator : BaseSamplePopupMediator<QuizeView>
    {
        [Inject]
        private QuizeModel quizeModel;

        private int indexQuestion;
        private Quize currentQuize;

        public override void Initialize()
        {
            base.Initialize();

            view.VariantClicked += OnVariantClicked;
        }

        public override void Dispose()
        {
            view.VariantClicked -= OnVariantClicked;

            base.Dispose();
        }

        public override void Appear()
        {
            base.Appear();

            indexQuestion = 0;
            currentQuize = quizeModel.CurrentQuize;

            SetQuestion(indexQuestion);
        }

        private void OnVariantClicked(int idx)
        {
            var res = (currentQuize.Questions[indexQuestion].Variants[idx] == currentQuize.Questions[indexQuestion].CorrectVariant);
            view.SetVariantState(idx, res ? QuizeState.Valid : QuizeState.Invalid);

            StartCoroutine(SpecialDelay());
        }

        private IEnumerator SpecialDelay()
        {
            yield return new WaitForSeconds(1);

            if (indexQuestion + 1 < currentQuize.Questions.Count)
                SetQuestion(++indexQuestion);
        }

        private void SetQuestion(int idx)
        {
            view.ResetState();
            view.SetQuestion(currentQuize.Questions[idx]);
            view.SetNumberQuiest(string.Format("{0}/{1}", idx + 1, currentQuize.Questions.Count));
        }
    }
}