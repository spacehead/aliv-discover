﻿using App.UI.Impl.Base;
using UnityEngine.UI;
using App.UI.Impl.Popups.Adding;
using App.UI.Impl.Popups.Data;
using System;
using App.Models.Data;

namespace App.UI.Impl.Popups
{
    public class QuizeView : BaseSamplePopupView
    {
        public QuizeMark Mark;
        public Button closeButton;
        public Button skipButton;
        public QuizeButton[] Buttons;
        public Text QuestionName;
        public Text NumberQuestion;

        public Action<int> VariantClicked; 

        public override void Initialize()
        {
            base.Initialize();
            for (int i = 0; i != Buttons.Length; ++i)
            {
                int idx = i;
                Buttons[i].ActiveBtn.onClick.AddListener(() =>
                {
                    OnVariantClicked(idx);
                });
            }
        }

        public override void Dispose()
        {
            for (int i = 0; i != Buttons.Length; ++i)
                Buttons[i].ActiveBtn.onClick.RemoveAllListeners();
            
            base.Dispose();
        }

        public void ResetState()
        {
            Mark.SetMarkState(QuizeState.Nope);
            for (int i = 0; i != Buttons.Length; ++i)
            {
                Buttons[i].SetInteractable(true);
                Buttons[i].SetState(QuizeState.Nope);
            }
        }

        public void SetQuestion(Question question)
        {
            QuestionName.text = question.QuestionTitle;
            Mark.SetPoints(question.AmountPoints);
            HideVariants();

            char alpha = 'A';
            for (int i = 0, im = Math.Min(question.Variants.Length, Buttons.Length); i != im; ++i)
            {
                Buttons[i].SetInteractable(true);
                Buttons[i].SetState(QuizeState.Nope);
                Buttons[i].SetVisible(true);

                Buttons[i].SetVariantText(string.Format("{0}. {1}", alpha++, question.Variants[i]));
            }
        }

        public void SetVariantState(int idx, QuizeState state)
        {
            Mark.SetMarkState(state);

            if (idx >= 0 && idx < Buttons.Length)
            {
                SetInteractableVariatns(false);
                Buttons[idx].SetState(state);
            }
        }

        public void SetNumberQuiest(string value)
        {
            NumberQuestion.text = value;
        }

        private void OnVariantClicked(int idx)
        {
            SetInteractableVariatns(false);

            if (VariantClicked != null)
                VariantClicked(idx);
        }

        private void HideVariants()
        {
            for (int i = 0; i != Buttons.Length; ++i)
                Buttons[i].SetVisible(false);
        }

        private void SetInteractableVariatns(bool state)
        {
            for (int i = 0; i != Buttons.Length; ++i)
                Buttons[i].SetInteractable(state);
        }
    }
}