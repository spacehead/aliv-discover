﻿using App.Signals;
using UnityEngine;
using Zenject;

namespace App.UI.Api
{

    public abstract class AMonoMediator : MonoBehaviour, IMediator
    {
        public abstract void Appear();
        public abstract void Disappear();
        public abstract void Dispose();
        public abstract string GetPopupName();
        public abstract void Hide();
        public abstract void Initialize();
        public abstract void Background();
        public abstract void Foreground();
        public abstract void OpenPopup(string name);
        public abstract void OpenScreen(string name);
        public abstract void SetLastSibling();
        public abstract void Show();
        public abstract bool IsNativeView();
    }

    public abstract class AMonoMediator<T> : AMonoMediator where T : AView
    {
        [SerializeField]
        protected T view;

        [Inject]
        protected OpenPopupSignal openPopupSignal;

        [Inject]
        protected OpenScreenSignal openScreenSignal;

        [Inject]
        protected ClosePopupSignal closePopupSignal;

        [Inject]
        protected WaitClosePopupSignal waitPopupSignal;

        //[Inject]
        public override void Initialize()
        {
            view.Initialize();
        }

        public override void SetLastSibling()
        {
            view.SetLastSibling();
        }

        public override string GetPopupName()
        {
            return view.GetGO().name;
        }

        public override void Show()
        {
            view.Show();
        }

        public override void Hide()
        {
            view.Hide();
        }

        public override bool IsNativeView()
        {
            return false;
        }

        public override void Background()
        {
        }

        public override void Foreground()
        {
        }

        public override void Dispose()
        {
            view.Dispose();
        }

        public override void OpenPopup(string name)
        {
            openPopupSignal.Fire(name);
        }

        public override void OpenScreen(string name)
        {
            openScreenSignal.Fire(name);
        }
        
        protected virtual void SendClosePopup()
        {
            closePopupSignal.Fire(this);
        }

        protected virtual void WaitClosePopup()
        {
            waitPopupSignal.Fire(this);
        }
    }
}
