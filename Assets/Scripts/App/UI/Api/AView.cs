﻿using System;
using UnityEngine;
using Zenject;

namespace App.UI.Api
{
    public abstract class AView : MonoBehaviour, IInitializable, IDisposable
    {
        protected GameObject cachedGO;
        protected RectTransform cachedTransform;

        public virtual void Initialize()
        {
            cachedTransform = GetComponent<RectTransform>();
            cachedGO = gameObject;
        }

        public virtual void Dispose()
        {
            Destroy(cachedGO);
        }

        public virtual void Show()
        {
            SetVisible(true);
        }

        public virtual void Hide()
        {
            SetVisible(false);
        }

        public void SetLastSibling()
        {
            cachedTransform.SetAsLastSibling();
        }

        public GameObject GetGO()
        {
            cachedGO = cachedGO ?? gameObject;
            return cachedGO;
        }

        public RectTransform GetTransform()
        {
            return cachedTransform;
        }

        protected void SetVisible(bool state)
        {
            cachedGO.SetActive(state);
        }

        public bool IsVisible()
        {
            return cachedGO.activeSelf;
        }

        protected void ActivateDelegate(Action callback)
        {
            if (callback != null)
                callback();
        }
    }
}