﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace App.UI.Api
{
    public abstract class BaseScope<T> : MonoBehaviour, IInitializable where T : IMediator
    {
        [SerializeField]
        private Transform shadeTransform;
        private GameObject shadeObject;
        
        [SerializeField]
        protected List<T> scope;

        public virtual void Initialize()
        {
            shadeObject = shadeTransform.gameObject;
            scope = new List<T>();
        }

        public T GetItem(string name)
        {
            return scope.Find(x => x.GetPopupName().Equals(name));
        }

        public List<T> GetItems()
        {
            return scope;
        }
        
        public void SetVisibleShade(bool state)
        {
            shadeObject.SetActive(state);
        }

        public void SetSiblingShade()
        {
            shadeTransform.SetAsLastSibling();
        }
    }
}