﻿using App.Signals;
using Zenject;

namespace App.UI.Api
{
    public abstract class ANativeMediator : IMediator
    {
        [Inject]
        protected OpenPopupSignal openPopupSignal;

        [Inject]
        protected OpenScreenSignal openScreenSignal;

        [Inject]
        protected ClosePopupSignal closePopupSignal;

        //[Inject]
        public virtual void Initialize()
        { }

        public virtual void Dispose()
        { }

        public abstract void Appear();
        public abstract void Disappear();


        public bool IsNativeView()
        {
            return true;
        }

        public virtual void Background()
        {
        }

        public virtual void Foreground()
        {
        }

        public abstract string GetPopupName();

        public void OpenPopup(string name)
        {
            openPopupSignal.Fire(name);
        }

        public void OpenScreen(string name)
        {
            openScreenSignal.Fire(name);
        }
        
        protected virtual void SendClosePopup()
        {
            closePopupSignal.Fire(this);
        }

        public void SetLastSibling()
        { }

        public void Show()
        { }

        public void Hide()
        { }
    }
}
