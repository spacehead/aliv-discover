﻿using App.Signals;
using UnityEngine;
using Zenject;

namespace App.UI.Api
{
    public abstract class AMediator<T> : IMediator where T : AView
    {
        [SerializeField]
        protected T view;

        [Inject]
        protected OpenPopupSignal openPopupSignal;

        [Inject]
        protected OpenScreenSignal openScreenSignal;

        //[Inject]
        public virtual void Initialize()
        {
            view.Initialize();
        }

        public virtual void SetLastSibling()
        {
            view.SetLastSibling();
        }

        public virtual string GetPopupName()
        {
            return view.GetGO().name;
        }

        public virtual void Show()
        {
            view.Show();
        }

        public virtual void Hide()
        {
            view.Hide();
        }

        public virtual void Dispose()
        {
            view.Dispose();
        }

        public virtual void Background()
        {
        }

        public virtual void Foreground()
        {
        }

        public abstract bool IsNativeView();

        public abstract void Appear();
        public abstract void Disappear();

        public void OpenPopup(string name)
        {
            openPopupSignal.Fire(name);
        }

        public void OpenScreen(string name)
        {
            openScreenSignal.Fire(name);
        }
    }
}
