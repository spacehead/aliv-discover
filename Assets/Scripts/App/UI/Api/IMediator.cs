﻿using System;
using Zenject;

namespace App.UI.Api
{
    public interface IMediator : IInitializable, IDisposable
    {
        bool IsNativeView();     

        void Appear();
        void Disappear();

        void Show();
        void Hide();

        void Background();
        void Foreground();

        string GetPopupName();    
        void SetLastSibling();

        void OpenPopup(string name);
        void OpenScreen(string name);
    }
}