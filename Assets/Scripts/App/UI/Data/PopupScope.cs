﻿using App.UI.Api;
using Zenject;
using System.Collections.Generic;
using UnityEngine;

namespace App.UI.Data
{
    public class PopupScope : BaseScope<IMediator>
    {
        [SerializeField]
        private List<AMonoMediator> popups;

        public override void Initialize()
        {
            base.Initialize();

            var list = GetItems();
            foreach (var item in popups)
                list.Add(item);
        }
        
        public void SetMonoPopups(List<AMonoMediator> list)
        {
            popups = list;
        }
    }
}