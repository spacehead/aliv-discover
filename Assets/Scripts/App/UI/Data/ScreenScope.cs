﻿using System.Collections.Generic;
using App.UI.Api;
using UnityEngine;

namespace App.UI.Data
{
    public class ScreenScope : BaseScope<IMediator>
    {
        [SerializeField]
        private List<AMonoMediator> screens;
        
        public override void Initialize()
        {
            base.Initialize();

            var list = GetItems();
            foreach (var item in screens)
                list.Add(item);
        }
    }
}