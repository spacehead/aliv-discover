﻿using System;
using Zenject;
using App.Signals;
using System.Collections.Generic;
using App.UI.Data;
using App.UI.Api;

namespace App.UI
{
    public class PopupManager : IInitializable, IDisposable
    {
        private bool isLockActive;
        private Stack<IMediator> queuePopups;

        private Queue<string> queueForOpen;

        [Inject]
        private PopupScope popupScore;

        [Inject]
        private OpenPopupSignal openPopupSignal;

        [Inject]
        private ClosePopupSignal closePopupSignal;

        [Inject]
        private WaitClosePopupSignal waitClosePopupSignal;

        [Inject]
        private PopupAnimationSignal popupAnimSignal;

        [Inject]
        private CloseAllPopupsSignal closeAllPopupSignals;

        [Inject]
        private ScreenToBackgroundSignal screenToBackground;

        public void Initialize()
        {
            queuePopups = new Stack<IMediator>();
            queueForOpen = new Queue<string>();

            openPopupSignal += OpenPopup;
            popupAnimSignal += PopupAnimationState;
            closePopupSignal += OnCloseLastPopup;
            waitClosePopupSignal += OnWaitClosePopup;
            closeAllPopupSignals += OnCloseAllPopup;


            OpenPopup(PopupLibrary.QuizePopup);
        }

        public void Dispose()
        {
            openPopupSignal -= OpenPopup;
            popupAnimSignal -= PopupAnimationState;
            closePopupSignal -= OnCloseLastPopup;
            waitClosePopupSignal -= OnWaitClosePopup;
            closeAllPopupSignals -= OnCloseAllPopup;

            queuePopups.Clear();
            queuePopups = null;
        }
        
        public bool HasOpenPopup()
        {
            return queuePopups.Count > 0;
        }

        public void CloseLastPopup()
        {
            OnCloseLastPopup();
        }

        private void OnCloseAllPopup()
        {
            while (HasOpenPopup())
                OnCloseLastPopup();
        }

        private void OnCloseLastPopup(IMediator mediator = null)
        {
            if (isLockActive || !HasOpenPopup())
                return;
            
            var popup = queuePopups.Pop();
            popup.Disappear();

            if (queuePopups.Count > 0 && !popup.IsNativeView())
                queuePopups.Peek().Foreground();
            else if (!popup.IsNativeView())
                screenToBackground.Fire(false);
        }

        private void OnWaitClosePopup(IMediator mediator)
        {
            if (queuePopups.Count > 0)
                queuePopups.Peek().Background();
            
            queuePopups.Push(mediator);
        }
        
        private void SetSiblingShade()
        {
            popupScore.SetSiblingShade();
        }

        private void SetVisibleShade(bool state)
        {
            isLockActive = state;
            popupScore.SetVisibleShade(state);
        }

        private void OpenPopup(string name)
        {
            if (isLockActive)
            {
                queueForOpen.Enqueue(name);
                return;
            }
                        
            var popup = popupScore.GetItem(name);
            if (popup == null)
                return;

            if (queuePopups.Count > 0 && !popup.IsNativeView())
                queuePopups.Peek().Background();
            else if (!popup.IsNativeView())
                screenToBackground.Fire(true);

            queuePopups.Push(popup);
            popup.SetLastSibling();
            popup.Appear();
        }

        private void PopupAnimationState(bool state)
        {
            SetVisibleShade(state);

            if (state)
                SetSiblingShade();
            else
                CheckQueueForOpen();
        }

        private void CheckQueueForOpen()
        {
            if (queueForOpen.Count > 0)
                OpenPopup(queueForOpen.Dequeue());
        }
    }
}