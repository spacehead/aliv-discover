﻿using System;
using App.Signals;
using App.UI.Api;
using App.UI.Data;
using UnityEngine;
using Zenject;

namespace App.UI
{
    public class ScreenManager : ITickable, IInitializable, IDisposable
    {
        [Inject]
        private PopupManager popupManager;

        [Inject]
        private OpenScreenSignal openScreenSignal;

        [Inject]
        private ScreenAnimationSignal screenAnimSignal;

        [Inject]
        private CloseScreenSignal closeScreenSignal;

        [Inject]
        private ScreenToBackgroundSignal screenToBackground;

        [Inject]
        private ScreenScope screenScope;
        
        private IMediator backScreen;
        private IMediator currentScreen;

        public void Initialize()
        {
            //currentScreen = screenScope.GetItem(ScreenLibrary.LoadingScreen);
            openScreenSignal += OnOpenScreenSignal;

            screenAnimSignal += ScreenAnimationState;
            closeScreenSignal += OnCloseScreenSignal;
            screenToBackground += OnScreenToBackground;
        }

        public void Dispose()
        {
            backScreen = null;
            currentScreen = null;

            screenAnimSignal -= ScreenAnimationState;
            openScreenSignal -= OnOpenScreenSignal;
            closeScreenSignal -= OnCloseScreenSignal;
            screenToBackground -= OnScreenToBackground;
        }

        public void Tick()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && popupManager.HasOpenPopup())
                popupManager.CloseLastPopup();
        }

        private void OnOpenScreenSignal(string screenName)
        {
            backScreen = currentScreen;

            currentScreen = screenScope.GetItem(screenName);
            if (currentScreen != null)
            {
                currentScreen.SetLastSibling();
                SetSiblingShade();

                SetVisibleShade(true);
                currentScreen.Appear();
            }
        }

        private void OnCloseScreenSignal()
        {
            if (currentScreen != null)
                currentScreen.Disappear();
        }

        private void OnScreenToBackground(bool isMoveToBackground)
        {
            if (currentScreen == null)
                return;

            if (isMoveToBackground)
                currentScreen.Background();
            else
                currentScreen.Foreground();
        }

        private void ScreenAnimationState(bool isAnimation)
        {
            if (isAnimation)
                return;
            
            SetVisibleShade(false);

            if (backScreen != null)
                backScreen.Disappear();
            backScreen = null;
        }

        private void SetSiblingShade()
        {
            screenScope.SetSiblingShade();
        }

        private void SetVisibleShade(bool state)
        {
            screenScope.SetVisibleShade(state);
        }
    }
}