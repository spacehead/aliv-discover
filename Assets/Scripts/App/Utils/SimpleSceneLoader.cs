﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SimpleSceneLoader : MonoBehaviour
{
    public Slider ProgressBar;

    private AsyncOperation async = null;

    private IEnumerator LoadLevel(int number)
    {
        async = SceneManager.LoadSceneAsync(number);
        yield return async;
    }

    private void Start()
    {
        ProgressBar.value = 0;
        StartCoroutine(LoadLevel(1));
    }

    private void Update()
    {
        if (async != null)
           ProgressBar.value = async.progress + 0.2f;
    }
}
