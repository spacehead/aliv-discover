using App.Models;
using App.Signals;
using App.UI;
using Zenject;

namespace App.Installer
{
    public class AppInstaller : MonoInstaller<AppInstaller>
    {
        public override void InstallBindings()
        {
            AppSignalsScope();
            AppModelsScope();
            AppServicesScope();
            AppManagersScope();
        }

        private void AppServicesScope()
        {
        }

        private void AppManagersScope()
        {
            Container.BindInterfacesAndSelfTo<PopupManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<ScreenManager>().AsSingle();
        }

        private void AppModelsScope()
        {
            Container.Bind<QuizeModel>().AsSingle();
        }

        private void AppSignalsScope()
        {
            Container.DeclareSignal<OpenPopupSignal>();
            Container.DeclareSignal<OpenScreenSignal>();
            Container.DeclareSignal<ClosePopupSignal>();
            Container.DeclareSignal<WaitClosePopupSignal>();
            Container.DeclareSignal<CloseAllPopupsSignal>();
            Container.DeclareSignal<CloseScreenSignal>();
            Container.DeclareSignal<ScreenToBackgroundSignal>();

            Container.DeclareSignal<PopupAnimationSignal>();
            Container.DeclareSignal<ScreenAnimationSignal>();
        }
    }
}